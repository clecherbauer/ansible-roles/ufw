ufw
==========

Ansible playbook that installs and configures ufw on Debian

Role Variables
--------------

The desired behavior can be refined via variables.

Option | Description
--- | ---
`ufw_allow_ports` | List of port to allow (default: `22, 80, 443`)

Example Playbook
----------------

```yaml
# file: test.yml
- hosts: local
        
  roles: 
  
    - role: ufw
      ufw_allow_ports:
        - 123
        - 456
        - 789
```


Test the code
----------------

```yaml
# file: test.yml
- hosts: local
        
  roles: 
  
    - role: .
      ufw_allow_ports:
        - 123
        - 456
        - 789
```